FROM openjdk:11
VOLUME /tmp3
EXPOSE 8085
ADD target/addtendocker.jar addtendocker.jar
ENTRYPOINT ["java","-jar","addtendocker.jar"]
