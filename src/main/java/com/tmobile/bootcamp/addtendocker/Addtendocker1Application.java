package com.tmobile.bootcamp.addtendocker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Addtendocker1Application {

	public static void main(String[] args) {
		SpringApplication.run(Addtendocker1Application.class, args);
	}

}
