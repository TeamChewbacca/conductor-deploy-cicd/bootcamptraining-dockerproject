package com.tmobile.bootcamp.addtendocker;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/www.myapp.com")
public class api {
    
	@Autowired
    Service addService;

    public api(Service addService) {
        this.addService = addService;
    }
//API call to Add Number to 10
    @GetMapping("/{num}")
    public String addTen(@PathVariable("num") int num) throws JSONException {
        int output=addService.addTenService(num);
        JSONObject object = new JSONObject();
        return object.put("sum", String.valueOf(output)).toString();
    }



}
